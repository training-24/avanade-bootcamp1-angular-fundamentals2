import { inject } from '@angular/core';
import { Router, Routes } from '@angular/router';
import { AuthService } from './core/auth.service';

export const authGuard = () => {
  const authService = inject(AuthService)
  const router = inject(Router)
  // .... AuthService.isLogged???
  if (!authService.isLogged) {
    router.navigateByUrl('login')
  }
  return authService.isLogged
}


export const routes: Routes = [
  {
    path: 'demo1',
    loadComponent: () => import('./features/demo1/demo1.component')
      .then(file => file.Demo1Component),

    canActivate: [authGuard]
  },
  {
    path: 'demo2',
    loadComponent: () => import('./features/demo2/demo2.component'),
    data: { title: 'Edit'},
    canActivate: [authGuard]
  },
  {
    path: 'demoX',
    loadComponent: () => import('./features/demo2/demo2.component'),
    data: { title: 'Add'}
  },

  { path: 'demo3', loadComponent: () => import('./features/demo3/demo3.component') },
  { path: 'todolist', loadComponent: () => import('./features/todolist/todolist.component') },
  { path: 'todolist2', loadComponent: () => import('./features/todolist2/todolist2.component') },
  { path: 'settings', loadComponent: () => import('./features/settings/settings.component') },
  { path: 'users', loadComponent: () => import('./features/users/users.component') },
  { path: 'users/:userId', loadComponent: () => import('./features/user-details/user-details.component') },
  {
    path: 'uikit',
    loadComponent: () => import('./features/uikit/uikit.component'),
    children: [
      {
        path: 'page1',
        loadComponent: () => import('./features/uikit/components/page1.component'),
      },
      { path: 'page2', loadComponent: () => import('./features/uikit/components/page2.component') },
      { path: 'page3', loadComponent: () => import('./features/uikit/components/page3.component') },
      { path: '', redirectTo: 'page1', pathMatch: 'full'},

    ]
  },
  { path: '', redirectTo: 'demo1', pathMatch: 'full'},
];


