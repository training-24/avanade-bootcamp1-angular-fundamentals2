import { effect, inject, Injectable, signal } from '@angular/core';

export type Language = 'it' | 'en';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  value = signal<Language>('it');
  constructor() {

    const languageFromLocalStorage = localStorage.getItem('language')

    if (languageFromLocalStorage) {
      this.value.set(languageFromLocalStorage as Language)
    }

    effect(() => {
      localStorage.setItem('language', this.value())
    });
  }

  changeLanguage(lang: Language) {
    this.value.set(lang)
  }

}
