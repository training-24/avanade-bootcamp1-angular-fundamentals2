import { ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { TodolistService } from '../../features/todolist2/services/todolist.service';
import { LanguageService } from '../language.service';

@Component({
  selector: 'app-navbar',
  standalone: true,
  // changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    RouterLink,
    RouterLinkActive
  ],
  template: `

    <div class="navbar bg-base-100">
      <div class="flex-1">
        <a class="btn btn-ghost text-xl" routerLink="">daisyUI</a>
      </div>
        <ul class=" flex flex-wrap px-1 gap-3">
          <li routerLinkActive="active" routerLink="demo1"><a>Demo1</a></li>
          <li routerLinkActive="active" routerLink="demo2"><a>Demo2</a></li>
          <li routerLinkActive="active" routerLink="demo3"><a>Demo 3</a></li>
          <li routerLinkActive="active" routerLink="users"><a>Users</a></li>
          <li routerLinkActive="active" routerLink="todolist"><a>Todo List</a></li>
          <li routerLinkActive="active" routerLink="todolist2"><a>Todo List2</a></li>
          <li routerLinkActive="active" routerLink="uikit"><a>UIKIT</a></li>
          <li routerLinkActive="active" routerLink="settings"><a>Settings</a></li>
        </ul>
      
      {{languageService.value() === 'it' ? '🇮🇹' : '🇬🇧'}}
      - todo: {{todoListSrv.totalTodos()}}
    </div>
  `,
  styles: `
    .active {
      @apply text-orange-500;
    }
  `
})
export class NavbarComponent {
  languageService = inject(LanguageService);
  todoListSrv = inject(TodolistService)

  render() {
    console.log('render navbar')
  }
}
