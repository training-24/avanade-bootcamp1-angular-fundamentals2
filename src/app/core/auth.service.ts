import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLogged = true
  http = inject(HttpClient);

  getUsers = this.http
    .get<User[]>('https://jsonplaceholder.typicode.com/users')

  login() {
    // http
    // islogged= true
  }

  logout() {
    // islogged= false
  }
}
