import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, effect, inject, signal } from '@angular/core';
import { takeUntilDestroyed, toSignal } from '@angular/core/rxjs-interop';
import { delay, interval, map } from 'rxjs';

interface Todo {
  id: string;
  title: string;
  completed: boolean;
}

@Component({
  selector: 'app-todolist',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    <h1>Todo List (1 file)</h1>

    <!--error component-->
    @if (error()) {
      <div class="alert alert-error">AHIA! errore</div>
    }

    <!--todo message-->
    @if (noItems()) {
      <div>there are no items. Add one</div>
    } @else {
      <div>
        {{totalTodos()}} todos - missing {{missingTodos()}} todos
      </div>  
    }
    
    <!--form-->
    <input
      class="input input-bordered"
      placeholder="Add Todo"
      #inputRef type="text" (keydown.enter)="addTodo(inputRef)"
    >
    
    <!--todo-list-->
    @for (todo of todos(); track todo.id) {
      <li>
        <input 
          type="checkbox" 
          [checked]="todo.completed"
          (change)="toggleTodo(todo)"
        >
        {{todo.title}} ({{todo.id}})
        <i class="fa fa-trash" (click)="deleteTodo(todo)"></i>
      </li> 
    }
    
    <pre>{{todos() | json}}</pre>
  `,
  styles: ``
})
export default class TodolistComponent {
  http = inject(HttpClient)
  todos = signal<Todo[]>([])
  error = signal(false)

  totalTodos = computed(() => this.todos().filter(t => t.completed).length)
  missingTodos = computed(() => this.todos().filter(t => !t.completed).length)
  noItems = computed(() => {
      return this.todos().length === 0
  })

  constructor() {
    this.getAllTodos();

    /*
    // example of effects
    effect(() => {
      localStorage.setItem('todos', this.todos().toString())
    });
    effect(() => {
      console.log(this.error())
    });
   */
  }

  obs = interval(1000)
    .pipe(
      takeUntilDestroyed()
    )

  getAllTodos() {
    this.obs.subscribe()
    this.http.get<Todo[]>('http://localhost:3000/todos')
      .pipe(
        takeUntilDestroyed(),
      )
      .subscribe({
        next: res => {
          this.todos.set(res);
        },

        error: err => {
          this.error.set(err)
        },

        complete: () => {
          console.log('completed')
        }
      })
  }

  addTodo(input: HTMLInputElement) {
    this.error.set(false)
    const payload: Omit<Todo, 'id'> = {
      title: input.value,
      completed: false
    }
    this.http.post<Todo>('http://localhost:3000/todos', payload)
      .subscribe({
        next: newTodo => {
          this.todos.update(todos => {
            return [...todos, newTodo]
          })
          input.value = ''
        },
        error: err => {
          this.error.set(true)
        },
      })
  }

  deleteTodo(todoToRemove: Todo) {
    this.error.set(false)
    this.http.delete(`http://localhost:3000/todos/${todoToRemove.id}`)
      .subscribe({
        next: () => {
          this.todos.update(todos => {
            return todos.filter(t => t.id !== todoToRemove.id);
          })
        },
        error: err => {
          this.error.set(true)
        },
      })
  }

  toggleTodo(todoToToggle: Todo) {
    this.http.patch<Todo>(`http://localhost:3000/todos/${todoToToggle.id}`, {
      completed: !todoToToggle.completed
    })
      .subscribe(todoUpdated => {
        this.todos.update(todos => {
          return todos.map(t => {
            return t.id === todoToToggle.id ? todoUpdated : t
          })
        })
      })

  }
}
