import { Component, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LanguageService } from '../../core/language.service';

@Component({
  selector: 'app-settings',
  standalone: true,
  imports: [
    FormsModule
  ],
  template: `
    <p>
      settings works! {{languageService.value()}}
    </p>
    
    <button 
      class="btn" 
      (click)="languageService.changeLanguage('en')">EN</button>
    <button 
      class="btn" 
      (click)="languageService.changeLanguage('it')">IT</button>
    
  `,
  styles: ``
})
export default class SettingsComponent {
  languageService = inject(LanguageService)
}
