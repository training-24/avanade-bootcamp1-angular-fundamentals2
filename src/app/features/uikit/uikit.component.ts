import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-uikit',
  standalone: true,
  template: `
    <h1>uikit </h1>

    <div class="flex gap-3">
      <button class="btn" routerLink="page1">SUb Page1</button>
      <button class="btn" routerLink="page2">Sub Page2</button>
      <button class="btn" routerLink="/uikit/page3">Page 3</button>
    </div>

    <router-outlet />

  `,
  imports: [
    RouterOutlet,
    RouterLink
  ],
  styles: ``
})
export default class UikitComponent {

}
