import { Component } from '@angular/core';
import { CardComponent } from '../../../shared/components/card.component';

@Component({
  selector: 'app-page1',
  standalone: true,
  imports: [
    CardComponent
  ],
  template: `
  
    <h1>Card Demo</h1>
    
    <app-card 
      title="ONE"
      img="https://daisyui.com/images/stock/photo-1606107557195-0e29a4b5b4aa.jpg"
      icon="🔗"
      buttonLabel="Visit"
      (buttonClick)="visit()"
      (iconClick)="visit()"
      [tags]="['angular', 'react', 'vue']"
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aspernatur cum dolor doloremque eaque facere facilis id ipsum maiores nobis porro quasi recusandae, reiciendis repellendus similique ut veniam? Dicta, sit?
      
    </app-card>
    
    <app-card
      title="SIGN IN"
      centered
      buttonLabel="SignIn"
      (buttonClick)="login()"
    >
     
      <input type="text" class="input input-bordered">
      <input type="text" class="input input-bordered">
    </app-card>
    
  `,
  styles: ``
})
export default class Page1Component {
  visit() {
    window.open('http://www.google.com')
  }
  login() {
    window.alert('ciao!')
  }
}
