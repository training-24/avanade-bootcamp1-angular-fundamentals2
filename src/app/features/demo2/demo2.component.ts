import { Component, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LanguageService } from '../../core/language.service';

@Component({
  selector: 'app-demo2',
  standalone: true,
  imports: [],
  template: `
    <p>
      demo2 works! {{title}}
    </p>
  `,
  styles: ``
})
export default class Demo2Component {

  // activatedRoute = inject(ActivatedRoute)


  title = '';

  constructor(
    public activatedRoute: ActivatedRoute,
    public languageService: LanguageService
  ) {
    this.activatedRoute.data
      .subscribe(data => {
        console.log(data)
      })

  }

}

interface MyData {
  title: string;
}
