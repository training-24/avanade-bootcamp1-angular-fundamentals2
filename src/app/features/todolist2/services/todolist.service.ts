import { HttpClient } from '@angular/common/http';
import { computed, inject, Injectable, signal } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Observable } from 'rxjs';
import { Todo } from '../../../model/todo';

@Injectable({
  providedIn: 'root'
})
export class TodolistService {
  http = inject(HttpClient)
  todos = signal<Todo[]>([])
  error = signal(false)

  totalTodos = computed(() => this.todos().filter(t => t.completed).length)
  missingTodos = computed(() => this.todos().filter(t => !t.completed).length)
  noItems = computed(() => {
    return this.todos().length === 0
  })
  color = computed(() => this.noItems() ? 'red' : 'green')

  constructor() {
    console.log('Todo List Service')
  }
  getAllTodos() {
    this.http.get<Todo[]>('http://localhost:3000/todos')
      .pipe(
        takeUntilDestroyed(),
      )
      .subscribe({
        next: res => {
          this.todos.set(res);
        },

        error: err => {
          this.error.set(err)
        },

        complete: () => {
          console.log('completed')
        }
      })
  }


  addTodo(input: HTMLInputElement) {
    this.error.set(false)
    const payload: Omit<Todo, 'id'> = {
      title: input.value,
      completed: false
    }

    this.http.post<Todo>('http://localhost:3000/todos', payload)
      .subscribe({
        next: newTodo => {
          this.todos.update(todos => {
            return [...todos, newTodo]
          })
          input.value = ''
        },
        error: err => {
          this.error.set(true)
        },
      })
  }

  deleteTodo(todoToRemove: Todo) {
    this.error.set(false)
    this.http.delete(`http://localhost:3000/todos/${todoToRemove.id}`)
      .subscribe({
        next: () => {
          this.todos.update(todos => {
            return todos.filter(t => t.id !== todoToRemove.id);
          })
        },
        error: err => {
          this.error.set(true)
        },
      })
  }

  toggleTodo(todoToToggle: Todo) {
    this.http.patch<Todo>(`http://localhost:3000/todos/${todoToToggle.id}`, {
      completed: !todoToToggle.completed
    })
      .subscribe(todoUpdated => {
        this.todos.update(todos => {
          return todos.map(t => {
            return t.id === todoToToggle.id ? todoUpdated : t
          })
        })
      })

  }
}
