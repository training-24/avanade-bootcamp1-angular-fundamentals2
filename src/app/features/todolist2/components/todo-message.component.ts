import { ChangeDetectionStrategy, Component, computed, effect, inject, input, Input } from '@angular/core';
import { TodolistService } from '../services/todolist.service';

@Component({
  selector: 'app-todo-message',
  standalone: true,
  //  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [],
  template: `
    @if (isEmpty) {
      <div>there are no items. Add one</div>
    } @else {
      <div>
        {{totalTodos}} todos - missing {{missingTodos}} todos
      </div>
    }
  `,
  styles: ``
})
export class TodoMessageComponent {
  @Input() totalTodos = 0;
  @Input() missingTodos = 0;
  @Input() isEmpty = false;

}
