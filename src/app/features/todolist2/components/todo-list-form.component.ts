import { Component, EventEmitter, inject, Output } from '@angular/core';

@Component({
  selector: 'app-todo-list-form',
  standalone: true,
  imports: [],
  template: `
    <input
      class="input input-bordered"
      placeholder="Add Todo"
      #inputRef type="text" 
      (keydown.enter)="save.emit(inputRef)"
    >
  `,
  styles: ``
})
export class TodoListFormComponent {
  @Output() save = new EventEmitter<HTMLInputElement>()
}
