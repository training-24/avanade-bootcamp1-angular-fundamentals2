import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Todo } from '../../../model/todo';

@Component({
  selector: 'app-todo-list-items',
  standalone: true,
  imports: [],
  template: `
    @for (todo of todos; track todo.id) {
      <li>
        <input
          type="checkbox"
          [checked]="todo.completed"
          (change)="toggle.emit(todo)"
        >
        {{todo.title}} ({{todo.id}})
        <i class="fa fa-trash" (click)="delete.emit(todo)"></i>
      </li>
    }
  `,
  styles: ``
})
export class TodoListItemsComponent {
  @Input() todos: Todo[] = []
  @Output() delete = new EventEmitter<Todo>()
  @Output() toggle = new EventEmitter<Todo>()

}
