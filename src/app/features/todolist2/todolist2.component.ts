import { JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { ErrorMsgComponent } from '../../shared/components/error-msg.component';
import { TodoListFormComponent } from './components/todo-list-form.component';
import { TodoListItemsComponent } from './components/todo-list-items.component';
import { TodoMessageComponent } from './components/todo-message.component';
import { TodolistService } from './services/todolist.service';

@Component({
  selector: 'app-todolist2',
  standalone: true,
  imports: [
    JsonPipe,
    ErrorMsgComponent,
    TodoMessageComponent,
    TodoListFormComponent,
    TodoListItemsComponent
  ],
  template: `

    @if (todoListSrv.error()) {
      <app-error-msg>ahia! non funziona!</app-error-msg>
    }

    <div [style.color]="todoListSrv.color()"> EXAMPLE COMPUTED</div>

    <app-todo-message
      [totalTodos]="todoListSrv.totalTodos()"
      [missingTodos]="todoListSrv.missingTodos()"
      [isEmpty]="todoListSrv.noItems()"
    />

    <app-todo-list-form
      (save)="todoListSrv.addTodo($event)" />
    
    <app-todo-list-items
      [todos]="todoListSrv.todos()"
      (toggle)="todoListSrv.toggleTodo($event)" 
      (delete)="todoListSrv.deleteTodo($event)"
    />
   
    <pre>{{todoListSrv.todos() | json}}</pre>

  `,
  providers: [
    TodolistService,
  ]
})
export default class Todolist2Component {
  todoListSrv = inject(TodolistService)

  constructor() {
    this.todoListSrv.getAllTodos()
  }

}
