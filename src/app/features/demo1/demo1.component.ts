import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject, signal } from '@angular/core';
import { interval, map, Subscription } from 'rxjs';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo1',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    <h1>Users</h1>

    @for (user of users(); track user.id) {
      <li>{{ user.address.city }}</li>
    }

    <pre>{{users | json}}</pre>
   
  `,
  styles: ``
})
export class Demo1Component {
  http = inject(HttpClient)
  users = signal<User[]>([])
  sub: Subscription;

  constructor() {
    this.sub = interval(1000)
      .subscribe(value => {
        console.log(value)
      })

    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users', {
    })
      .subscribe((res) => {
        this.users.set(res)
      })
  }

  ngOnDestroy() {
    console.log('destroyed')
    this.sub.unsubscribe()
  }
}



