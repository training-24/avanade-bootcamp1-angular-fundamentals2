import { NgForOf } from '@angular/common';
import { Component, signal } from '@angular/core';
import { RouterLink } from '@angular/router';
import { count } from 'rxjs';

@Component({
  selector: 'app-users',
  standalone: true,
  imports: [
    RouterLink,
    NgForOf
  ],
  template: `
    
    <h1>NgFor</h1>
    <li *ngFor="let user of users()">{{user.name}}</li>
    
    <hr>
    
    <h1>for block</h1>
    @for (user of users(); track user.id) {
      <li [routerLink]="'/users/' + user.id">{{user.name}}</li>  
    }
    
    {{counter}}
  `,
  styles: ``
})
export default class UsersComponent {

  users = signal([
    { id: 1, name: 'fabio'},
    { id: 2, name: 'ciccio'},
    { id: 3, name: 'mario'},
  ])

  counter = 0;

  constructor() {
    setInterval(() => {
      this.counter++
    }, 1000)
  }

  protected readonly count = count;
}
