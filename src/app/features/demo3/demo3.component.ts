import { AsyncPipe, JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { interval, map } from 'rxjs';
import { AuthService } from '../../core/auth.service';
import { User } from '../../model/user';
import Todolist2Component from '../todolist2/todolist2.component';

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [
    JsonPipe,
    AsyncPipe,
    Todolist2Component
  ],
  template: `
    <h1>Demo http {{totalUser()}}</h1>

    <h1>counter: {{counter$ | async}}</h1>
    
    @for (user of authService.getUsers | async; track user.id) {
      <li>{{ user.address.city }}</li>
    }
    @for (user of users(); track user.id) {
      <li>{{ user.address.city }}</li>
    }

    <app-todolist2></app-todolist2>
    <app-todolist2></app-todolist2>
  `,
})
export default class Demo3Component {
  users = toSignal(
    inject(HttpClient)
      .get<User[]>('https://jsonplaceholder.typicode.com/users')
  )

  authService = inject(AuthService)

  counter$ = interval(1000)

  users$ = inject(HttpClient)
      .get<User[]>('https://jsonplaceholder.typicode.com/users');

  totalUser = computed(() => {
    return this.users()?.length
  })

}
