import { Component, inject, Input } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { filter, map } from 'rxjs';

@Component({
  selector: 'app-user-details',
  standalone: true,
  imports: [
    RouterLink,
    ReactiveFormsModule
  ],
  template: `
    <p>
      user-details works! {{id}} - {{userId}}
    </p>
    
    
    <button class="btn" routerLink="/users/1">1</button>
    <button class="btn" routerLink="../2">2</button>
    <button class="btn" routerLink="../3">3</button>
  `,
  styles: ``
})
export default class UserDetailsComponent {
  // 3. with Input
  @Input() userId: string | undefined

  // PRECEDENTE APPROCCIO
  activatedRoute = inject(ActivatedRoute);
  id: string | undefined

  constructor() {
    // 1. iMPERATIVO
    this.id = this.activatedRoute.snapshot.params['userId'];

    // 2. REATTIVE
    this.activatedRoute.params
      .pipe(
        map(params => params['userId']),
        filter(id => id > 1),
      )
      .subscribe(user => {
        console.log(user)
      })
  }
}
