import { Component } from '@angular/core';

@Component({
  selector: 'app-error-msg',
  standalone: true,
  imports: [],
  template: `
    <div class="alert alert-error">
      <ng-content></ng-content> 
    </div>
  `,
  styles: ``
})
export class ErrorMsgComponent {

}
