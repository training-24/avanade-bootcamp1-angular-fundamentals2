import { JsonPipe, NgClass } from '@angular/common';
import { booleanAttribute, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [
    JsonPipe,
    NgClass
  ],
  template: `
    <div class="card w-full bg-base-100 shadow-xl">
      @if(img) {
        <figure>
          <img 
            [src]="img" [alt]="title" />
        </figure>  
      }
      <div
        class="card-body"
        [ngClass]="{
          'items-center': centered
        }"
      >
        
        @if (title) {
          <div class="flex justify-between">
            <h2 class="card-title">
              {{ title }}
            </h2>
            <span (click)="iconClick.emit()">
              {{ icon }}
            </span>
          </div>
        }
        
        <ng-content></ng-content>

        @if (tags.length) {
          <div class="x card-actions justify-end">
            @for (tag of tags; track $index) {
              <div class="badge badge-outline">{{ tag }}</div>
            }
          </div>  
        }
        

        <div class="y card-actions justify-end">
          @if(buttonLabel) {
            <button
              (click)="buttonClick.emit()"
              class="btn btn-primary">
              {{ buttonLabel }}
            </button>  
          }
          
        </div>
      </div>
    </div>
  `,
  styles: ``
})
export class CardComponent {
  @Input({ required: true }) title = ''
  @Input() img = ''
  @Input() icon = ''
  @Input() buttonLabel = ''
  @Input({ transform: booleanAttribute }) centered: boolean = false;
  @Input() tags: string[] = [];
  @Output() iconClick = new EventEmitter()
  @Output() buttonClick = new EventEmitter()
}
