import { Component, inject } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { NavbarComponent } from './core/components/navbar.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, NavbarComponent],
  template: `
    <app-navbar />
    <hr>
    <router-outlet />
  `,
  providers: [

  ]
})
export class AppComponent {
  router = inject(Router)

  constructor() {
   /* console.log(this.router)
    this.router.events
      .subscribe(event => {
        console.log(event)
      })*/

  }
}
